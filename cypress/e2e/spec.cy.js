import 'cypress-xpath';

describe('AQI-Asia Quest', () => {
  const Url= "https://aqi.co.id/"
  beforeEach(() => {
    cy.viewport('macbook-13');
    cy.visit(Url);
    cy.wait(2000);
  });
  describe('Available Access to Company Services and Information', () => {
    it('Company Information', () => {
      cy.contains('Company').click();
      cy.wait(1000);
      cy.contains('Company Information').click();
      //validation
      cy.get('[data-hs-cos-type="rich_text"]').should('contain', 'About Us');
    });
    it('Service Company', () => {
      cy.contains('Service').click();
      cy.wait(1000);
      cy.contains('Information System DX Solution').click();
      //validation
      cy.get('[class="content"]').should('be.visible')
    });
  });
  describe('View Service details and Recruite Site', () => {
    it('View Service detail', () => {
      cy.contains('About DX Consulting').scrollIntoView();
      cy.contains('About DX Consulting').click();
      cy.contains('Inquiries about this matter').scrollIntoView();
      cy.contains('Inquiries about this matter').should('be.visible')
    });
    it('Recruite Site', () => {
      cy.visit('https://recruit.asia-quest.jp/contact/')
      cy.get('#hs-eu-confirmation-button').click();
      //type field
      cy.get('input[name="firstname"]').type('Setia');
      cy.get('input[name="lastname"]').type('Syalala');
      cy.get('input[name="email"]').type('test@mail.com');
      cy.get('input[name="phone"]').type('121212');
      cy.get('textarea[name="message"]').type('Test Automation');
      //confirmation
      cy.get('input[name="i_agree_to_the_privacy_policy"]').check();
      cy.get('.hs-button').click();
    });
  });
});